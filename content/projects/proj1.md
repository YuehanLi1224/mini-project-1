+++
title = "Project 1"
description = ""
+++ 

# Description:
Continuous Delivery of Personal Website

## Requirements
● Website built with Zola, Hugo, Gatsby, Next.js or equivalent</p>
● GitLab workflow to build and deploy site on push</p>
● Hosted on Vercel, Netlify, AWS Amplify, AWS S3, or others.</p>

## Grading Rubric
● Website Functionality (30 points)</p>
● GitLab Workflow (30 points)</p>
● Hosting and Deployment (20 points)</p>
● Documentation (10 points)</p>
● Demo Video (10 points)</p>
