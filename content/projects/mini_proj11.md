+++
title = "Mini-Project 11"
description = ""
+++ 

# Description:
Rust Serverless Transformer Endpoint

## Requirements
● Dockerize Hugging Face Rust transformer</p>
● Deploy container to AWS Lambda</p>
● Implement query endpoint</p>

## Grading Criteria
● Transformer packaging: 30%</p>
● Serverless deployment: 30%</p>
● Endpoint functionality: 30%</p>
● Documentation: 10%</p>

## Deliverables
● Dockerfile and Rust code</p>
● Screenshot of AWS Lambda</p>
● cURL request against endpoint</p>