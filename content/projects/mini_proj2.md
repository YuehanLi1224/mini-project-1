+++
title = "Mini-Project 2"
description = "https://gitlab.com/YuehanLi1224/mini-project2"
+++

# Description:
Create a simple AWS Lambda function that processes data.

## Requirements
● Rust Lambda Function using Cargo Lambda Links to an external site.</p>
● Process and transform sample data</p>

## Grading Criteria
● Lambda functionality: 30%</p>
● API Gateway integration: 30%</p>
● Data processing: 30%</p>
● Documentation: 10% </p>