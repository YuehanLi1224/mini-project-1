+++
title = "Mini-Project 8"
description = ""
+++ 

# Description:
Rust Command-Line Tool with Testing

## Requirements
● Rust command-line tool</p>
● Data ingestion/processing</p>
● Unit tests</p>

## Grading Criteria
● Tool functionality: 30%</p>
● Data processing: 30%</p>
● Testing implementation: 30%</p>
● Documentation: 10%</p>

## Deliverables
● Rust tool source code</p>
● Sample output</p>
● Testing report</p>