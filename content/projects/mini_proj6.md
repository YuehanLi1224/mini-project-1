+++
title = "Mini-Project 6"
description = "https://gitlab.com/YuehanLi1224/mini_project6"
+++ 

# Description:
Instrument a Rust Lambda Function with Logging and Tracing

## Requirements
● Add logging to a Rust Lambda function</p>
● Integrate AWS X-Ray tracing</p>
● Connect logs/traces to CloudWatch</p>

## Grading Criteria
● Logging implementation: 30%</p>
● X-Ray tracing: 30%</p>
● CloudWatch centralization: 30%</p>
● Documentation: 10%</p>

## Deliverables
● Updated Rust code</p>
● Screenshots or demo video showing logs and traces</p>