+++
title = "Mini-Project 4"
description = "https://gitlab.com/YuehanLi1224/mini_project4"
+++ 

# Description:
Containerize a Rust Actix Web Service

## Requirements
● Containerize simple Rust Actix web app</p>
● Build Docker image</p>
● Run container locally</p>

## Grading Criteria
● Container functionality: 40%</p>
● Dockerfile and build: 40%</p>
● Documentation: 20%</p>

## Deliverables
● Dockerfile</p>
● Screenshots or demo video showing container running</p>
● Writeup of process</p>