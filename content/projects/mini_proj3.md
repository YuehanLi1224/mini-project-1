+++
title = "Mini-Project 3"
description = "https://gitlab.com/YuehanLi1224/mini-proj3"
+++ 

# Description:
Create an S3 Bucket using CDK with AWS CodeWhisperer.

## ## Requirements
● Create S3 bucket using AWS CDK</p>
● Use CodeWhisperer to generate CDK code</p>
● Add bucket properties like versioning and encryption</p>

## Grading Criteria
● Correct CDK bucket creation: 30%</p>
● Use of CodeWhisperer: 30%</p>
● Bucket properties: 30%</p>
● Documentation: 10%</p>

## Deliverables
● CDK app code</p>
● Generated S3 bucket screenshot</p>
● Writeup explaining CodeWhisperer usage</p>