+++
title = "Mini-Project 10"
description = ""
+++ 

# Description:
Serverless ETL Pipeline with Rust Polars

## Requirements
●Extract and transform data with Rust Polars</p>
● Orchestrate ETL steps using Step Functions</p>
● Load processed data into S3</p>

## Grading Criteria
●Rust Polars data processing: 30%</p>
● Step Functions workflow: 30%</p>
● S3 data loading: 30%</p>
● Documentation: 10%</p>

## Deliverables
● Rust Polars ETL code</p>
● Step Functions workflow screenshot</p>
● Sample transformed S3 data</p>