+++
title = "Project 4"
description = ""
+++ 

# Description:
Rust AWS Lambda and Step Functions

## Requirements
● Rust AWS Lambda function</p>
● Step Functions workflow coordinating Lambdas</p>
● Orchestrate data processing pipeline</p>

## Grading Rubric
● Rust Lambda Functionality (30 points)</p>
● Step Functions Workflow (30 points)</p>
● Data Processing Pipeline (20 points)</p>
● Documentation (10 points)</p>
● Demo Video (10 points)</p>