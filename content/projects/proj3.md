+++
title = "Project 3"
description = ""
+++ 

# Description:
Rust Vector Database or Polar DataFrame API

## Requirements
● High-performance vector database or Polars Dataframe invoked in Rust</p>
● Data ingestion and query functionality</p>
● Benchmarks API Performance</p>

## Grading Rubric
● Vector Database or Polars DataFrame Implementation (40 points)</p>
● Data Ingestion and Queries (30 points)</p>
● Benchmarking (10 points)</p>
● Documentation (10 points)</p>
● Demo Video (10 points)</p>