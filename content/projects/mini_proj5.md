+++
title = "Mini-Project 5"
description = "https://gitlab.com/YuehanLi1224/mini_project5"
+++ 

# Description:
Serverless Rust Microservice

## Requirements
● Create a Rust AWS Lambda function</p>
● Implement a simple service</p>
● Connect to a database</p>

## Grading Criteria
● Rust Lambda functionality: 30%</p>
● Database integration: 30%</p>
● Service implementation: 30%</p>
● Documentation: 10%</p>

## Deliverables:
● Rust code</p>
● Screenshots or demo video showing successful invocation</p>
● Writeup explaining service</p>