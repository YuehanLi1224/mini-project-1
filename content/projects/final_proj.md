+++
title = "Team Project"
description = ""
+++ 

# Description:
LLMOps - Model Serving with Rust

## Requirements
● Obtain open source ML model</p>
● Create Rust web service for model inferences</p>
● Containerize service and deploy to Kubernetes</p>
● Implement CI/CD pipeline</p>

## Grading Rubric
● Rust Model Serving (25 points)</p>
● Dockerization and Kubernetes (25 points)</p>
● CI/CD Pipeline (25 points)</p>
● Monitoring and Metrics (10 points)</p>
● Documentation (5 points)</p>
● Demo Video (10 points)</p>
