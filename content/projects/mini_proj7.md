+++
title = "Mini-Project 7"
description = ""
+++ 

# Description:
Data Processing with Vector Database

## Requirements
● Ingest data into Vector database</p>
● Perform queries and aggregations</p>
● Visualize output</p>

## Grading Criteria
● Data ingestion: 30%</p>
● Query functionality: 30%</p>
● Visualizations: 30%</p>
● Documentation: 10%</p>

## Deliverables
● Rust code
● Screenshots or demo video showing successful invocation</p>
● Writeup explaining service</p>