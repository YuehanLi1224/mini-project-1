+++
title = "Project 2"
description = ""
+++ 

# Description:
Continuous Delivery of Rust Microservice

## Requirements
● Simple REST API/web service in Rust</p>
● Dockerfile to containerize service</p>
● CI/CD pipeline files</p>

## Grading Rubric
● Rust Microservice Functionality (30 points)</p>
● Docker Configuration (20 points)</p>
● CI/CD Pipeline (30 points)</p>
● Documentation (10 points)</p>
● Demo Video (10 points)</p>