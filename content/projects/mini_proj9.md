+++
title = "Mini-Project 9"
description = ""
+++ 

# Description:
Rust Lambda Data Processing Pipeline

## Requirements
● Series of Rust Lambda functions</p>
● Implement data transformations</p>
● Orchestrate pipeline with Step Functions</p>

## Grading Criteria
● Lambda functionality: 30%</p>
● Data transformations: 30%</p>
● Pipeline orchestration: 30%</p>
● Documentation: 10%</p>

## Deliverables
● Rust code</p>
● Screenshots and architecture diagrams</p>
● Explanation writeup</p>