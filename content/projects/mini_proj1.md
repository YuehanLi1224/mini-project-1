+++
title = "Mini-Project 1"
description = "https://gitlab.com/YuehanLi1224/mini-project-1/-/tree/main"
+++ 

# Description:
Create a static site with Zola Links to an external site., a Rust static site generator, that will hold all of the portfolio work in this class.  Store source code in a GitLab repo in our Duke GitLab organization.

## Requirements
● Static site generated with Zola</p>
● Home page and portfolio project page templates</p>
● Styled with CSS</p>
● GitLab repo with source code</p>

## Deliverables
● Link to live static site or screenshot showing it running</p>
● Link to GitLab repo</p>

## Grading Criteria
● Correct Zola usage: 25%</p>
● Page templates: 35%</p>
● Styling: 20%</p>
● GitLab repo: 20%</p> 
