+++
title = "Mini-Project 14"
description = ""
+++ 

# Description:
Evaluating Code Generation Tools

## Requirements
● Generate calculator code with 3 different tools (AWS CodeWhisperer, An Open Source LLMs like Star Coder, Gemni)</p>
● Implement generated code</p>
● Unit test each calculator function</p>

## Grading Criteria
● Code generation integration: 30%</p>
● Unit testing implementations: 40%</p>
● Comparison analysis: 20%</p>
● Documentation: 10%</p>

## Deliverables
● Generated code samples</p>
● Unit testing code</p>
● Report comparing tools</p>