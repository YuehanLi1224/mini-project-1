+++
title = "Mini-Project 12"
description = ""
+++ 

# Description:
Candle Transformer Model Serving

## Requirements
● Candle + Hugging Face Rust model</p>
● Containerize prediction endpoint (advanced students can create single file binary)</p>
● Query model endpoint</p>

## Grading Criteria
● Model building: 30%</p>
● Containerization: 30%</p>
● Prediction queries: 30%</p>
● Documentation: 10%</p>