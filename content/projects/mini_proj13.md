+++
title = "Mini-Project 13"
description = ""
+++ 

# Description:
Fine-Tuned BERT Classifier

## Requirements
● Take base BERT model</p>
● Add task-specific fine-tuning</p>
● Evaluate model metrics</p>

## Grading Criteria
● Fine-tuning implementation: 30%</p>
● Performance benchmarking: 30%</p>
● Model analysis: 30%</p>
● Documentation: 10%</p>
