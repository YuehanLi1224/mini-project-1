+++
title = "Home Page"
template = "index.html"
+++

# I. About IDS 721

## Course Name: 
Cloud Computing for Data Analysis
## Term: 
Spring 2024
## Date: 
01/10/2024 - 04/17/2024
## Time:
We 3:20-5:50PM
## Office Hours: 
Before and after class and as needed

## Structure:

● 3:20-4:00 Class Discussion on Reading</p>
● 4:00-4:45 - Teach</p>
● 4:45-5:00 - Break</p>
● 5:00-6:00 - Lab</p>

## Course Description:

This course is designed to give you a comprehensive view of cloud computing including
Big Data, Machine Learning and Large Language Models (LLMS). A variety of learning
resources will be used including interactive labs on AWS. This is a project-based

## Learning Objectives:

Upon successful completion of this course, you will be able to:
1. Summarize the fundamentals of cloud computing
2. Achieve AWS Solutions Architect Certification
3. Master the Rust language for general programming
4. Effectively utilize AI Pair Programming
5. Evaluate the economics of cloud computing
6. Accurately evaluate distributed computing challenges and opportunities and apply
this knowledge to real-world projects.
7. Develop non-linear life-long learning skills
8. Build, share and present compelling portfolios using: GitLab, Hugging Face,
YouTube, Linkedin and a personal portfolio website.
9. Develop Metacognition skills (By teaching we learn)

## Core Tech Stack:

● Rust</p>
● AWS (AWS Learner Labs, Optional Free Tier Account)</p>
● GitLab</p>
● Slack</p>
● AWS Lightsail for Research (GPU)</p>
● CodeWhisperer</p>

## Communication:

● Slack</p>
● GitLab</p>

## Media and Labs:

● Duke AIML Group GitLab</p>
● 52 Weeks of AWS-The Complete Series</p>
● AWS Academy Labs and Sandboxes</p>
● AWS Lightsail for Research (TBD)</p>
● Coursera Courses</p>

## Reading Material:

● Developing on AWS with C#</p>
● Implementing MLOps in the Enterprise</p>
● Practical MLOps</p>
● Python for DevOps</p>
● The Rust Programming Language online book.</p>
● Know Thyself: The Science of Self-Awareness</p>
● AWS Whitepapers and Certification PPTs</p>

# II. About Me

## Name:

Yuehan(Helen) Li </p> 

## Education:

Master of Science in ECE, Duke University, 2023-2025(expected)</p>
Bachelor of Engineering in Computer Science and Technology, NJUPT, 2019-2023</p>
