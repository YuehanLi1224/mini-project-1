Gitlab workflow:
![GitLab Workflow](gitlab_workflow.png)

Hosted on Netlify:
![Hosted on Netlify](netlify.png)

My website homepage:
![Homepage](homepage.png)

My website projects lists page:
![Projects List](projects_list.png)

My website project detail page, here I show the page for mini project 6:
![Project Page Detail](project_detail_page.png)

The demo video:
link: https://www.youtube.com/watch?v=o0zsumWOa64
